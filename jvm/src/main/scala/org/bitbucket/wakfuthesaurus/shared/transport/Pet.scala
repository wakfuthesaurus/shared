package org.bitbucket.wakfuthesaurus.shared.transport

final case class Pet(id: Int, refId: Int, gfx: Int)
