package org.bitbucket.wakfuthesaurus.shared.transport

sealed trait Effect {
  def id: Int
  def action: Int
  def areaShape: Int
  def areaSize: Seq[Int]
  def params: Seq[Double]
  def containerMinLevel: Int
  def containerMaxLevel: Int
  def probabilityBase: Double
  def probabilityInc: Double
  def displayInSpellDescription: Boolean
  def effectRequirement: Option[String]
}

final case class SingleEffect(
    override val id: Int,
    override val action: Int,
    override val areaShape: Int,
    override val areaSize: Seq[Int],
    override val params: Seq[Double],
    override val containerMinLevel: Int,
    override val containerMaxLevel: Int,
    override val probabilityBase: Double,
    override val probabilityInc: Double,
    override val displayInSpellDescription: Boolean,
    override val effectRequirement: Option[String]
) extends Effect

final case class EffectGroup(
    override val id: Int,
    override val action: Int,
    override val areaShape: Int,
    override val areaSize: Seq[Int],
    override val params: Seq[Double],
    override val containerMinLevel: Int,
    override val containerMaxLevel: Int,
    override val probabilityBase: Double,
    override val probabilityInc: Double,
    override val displayInSpellDescription: Boolean,
    override val effectRequirement: Option[String],
    children: Seq[Effect]
) extends Effect
