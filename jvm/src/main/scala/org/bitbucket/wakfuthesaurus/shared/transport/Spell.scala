package org.bitbucket.wakfuthesaurus.shared.transport

final case class Spell(
  id: Int,
  gfxId: Int,
  maxLevel: Int,
  maxCastPerTarget: Int,
  maxCastPerTurn: RelValue,
  element: Int,
  uiPosition: Int,
  singleTarget: Boolean,
  effects: Seq[Effect],
  criticalEffects: Seq[Effect],
  requireLos: Boolean,
  requireLine: Boolean,
  requireDiagonal: Boolean,
  apCost: RelValue,
  mpCost: RelValue,
  wpCost: RelValue,
  minRange: RelValue,
  maxRange: RelValue,
  useAutomaticDescription: Boolean,
  modifiableRange: Boolean,
  upgrades: Seq[Int]
)

final case class RelValue(base: Double, inc: Double)
