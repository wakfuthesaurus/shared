package org.bitbucket.wakfuthesaurus.shared.transport

final case class Item(
  id: Int,
  typeId: Int,
  rarity: Int,
  setId: Int,
  gfxId: Int,
  femaleGfxId: Int,
  level: Int,
  useInfo: ItemUseInfo,
  hmiActions: Seq[ApplyHmiAction],
  useEffects: Seq[Effect],
  equipEffects: Seq[Effect],
  visible: Boolean,
  isBeta: Boolean,
  baseCraft: Int
)

final case class ItemUseInfo(
  conditions: Seq[String],
  apCost: Int,
  mpCost: Int,
  wpCost: Int,
  minRange: Int,
  maxRange: Int,
  requireLos: Boolean,
  requireLine: Boolean
)
