package org.bitbucket.wakfuthesaurus.shared.transport

final case class State(
  id: Int,
  gfxId: String,
  maxLevel: Int,
  requirement: Option[String],
  effects: Seq[Effect]
)
