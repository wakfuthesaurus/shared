package org.bitbucket.wakfuthesaurus.shared.transport

final case class CharacterBreed(
  id: Int,
  definitions: Seq[CharacterBreedDefinition],
  spells: Seq[Int],
  support: Seq[Int],
  passive: Seq[Int]
)

final case class CharacterBreedDefinition(
  sex: Int,
  defaultSkinIndex: Int,
  defaultSkinFactor: Int,
  defaultHairIndex: Int,
  defaultHairFactor: Int,
  defaultPupilIndex: Int,
  skinColors: Seq[Color],
  hairColors: Seq[Color],
  pupilColors: Seq[Color]
)

final case class Color(red: Double, green: Double, blue: Double, alpha: Double)
